import psycopg2
import threading
from pyzabbix import ZabbixAPI
from datetime import datetime, date, timedelta
from send_mail import send_report
import sys
import time
import os


def get_data(stores, filename, start, finish):
    count = 0
    week = datetime.now().date().isocalendar()[1] - 1
    with open('reports/' + filename + '_' + str(week) + '_week.csv', 'w') as f:
        f.write("BO;bon_seq_id;timestamp;amount;count(*)\n")
    for i in range(len(stores)):
        try:
            conn = psycopg2.connect(host=stores[i]['ip'], dbname='postgres', user='gkretail', password='gkretail',
                                    connect_timeout=3)
            cursor = conn.cursor()
            cursor.execute("""
                  select bon_seq_id, timestamp, amount, error_description, count(*)
              from xrg_transport_module
              where sign is NULL and
              timestamp > '""" + str(datetime.fromtimestamp(start).strftime('%Y-%m-%d %H:%M:%S')) + """' and
              timestamp < '""" + str(datetime.fromtimestamp(finish).strftime('%Y-%m-%d %H:%M:%S')) + """'
              group by bon_seq_id, timestamp, amount, error_description
              order by timestamp""")
            records = cursor.fetchall()
            if records:
                with open('reports/' + filename + '_' + str(week) + '_week.csv', 'a') as f:
                    for j in records:
                        f.write(';'.join([stores[i]['name'], str(j[0]), str(j[1]), str(j[2]), str(j[4])]) + '\n')
            conn.close()
        except:
            pass
        count += 1


def get_zdata():
    z = ZabbixAPI('http://msk-dpro-app351')
    z.login("andrey.gavrilov", "200994")
    find = z.do_request('host.get', {
    'templateids': ['128513'],
    'selectInterfaces': ['ip'],
    'output': ['host']})['result']
    hosts = [{'name': i['host'], 'hostid': i['hostid'], 'ip': i['interfaces'][0]['ip']} for i in find]
    return hosts


def init_threads(hosts, start, finish):
    worker1 = hosts[:int(len(hosts)/10)]
    worker2 = hosts[int(len(hosts)/10):2*int(len(hosts)/10)]
    worker3 = hosts[2*int(len(hosts)/10):3*int(len(hosts)/10)]
    worker4 = hosts[3*int(len(hosts)/10):4*int(len(hosts)/10)]
    worker5 = hosts[4*int(len(hosts)/10):5*int(len(hosts)/10)]
    worker6 = hosts[5*int(len(hosts)/10):6*int(len(hosts)/10)]
    worker7 = hosts[6*int(len(hosts)/10):7*int(len(hosts)/10)]
    worker8 = hosts[7*int(len(hosts)/10):8*int(len(hosts)/10)]
    worker9 = hosts[8*int(len(hosts)/10):9*int(len(hosts)/10)]
    worker10 = hosts[9*int(len(hosts)/10):10*int(len(hosts)/10)]
    t1 = threading.Thread(target=get_data, args=(worker1, 'Thread1', start, finish))
    t2 = threading.Thread(target=get_data, args=(worker2, 'Thread2', start, finish))
    t3 = threading.Thread(target=get_data, args=(worker3, 'Thread3', start, finish))
    t4 = threading.Thread(target=get_data, args=(worker4, 'Thread4', start, finish))
    t5 = threading.Thread(target=get_data, args=(worker5, 'Thread5', start, finish))
    t6 = threading.Thread(target=get_data, args=(worker6, 'Thread6', start, finish))
    t7 = threading.Thread(target=get_data, args=(worker7, 'Thread7', start, finish))
    t8 = threading.Thread(target=get_data, args=(worker8, 'Thread8', start, finish))
    t9 = threading.Thread(target=get_data, args=(worker9, 'Thread9', start, finish))
    t10 = threading.Thread(target=get_data, args=(worker10, 'Thread10', start, finish))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t6.start()
    t7.start()
    t8.start()
    t9.start()
    t10.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()
    t6.join()
    t7.join()
    t8.join()
    t9.join()
    t10.join()


def get_dates():
    year = datetime.now().date().isocalendar()[0]
    week = datetime.now().date().isocalendar()[1] - 1
    d = date(year,1,1)
    d = d - timedelta(d.weekday())
    dlt = timedelta(days = (week-1)*7)
    return int(time.mktime((d + dlt).timetuple())),  int(time.mktime((d + dlt + timedelta(days=6)).timetuple()) + 24*3600 - 1)


def compile():
    week = datetime.now().date().isocalendar()[1] - 1
    thread = []
    for file in os.listdir(os.getcwd()+ '/reports'):
        if '_week.csv' in file:
            thread.append(os.getcwd()+ '/reports/' + file)
    with open('reports/' + str(week) + '_week_db_summery.csv', 'w') as f:
        f.write('week {0}\n'.format(week))
        f.write("BO;bon_seq_id;timestamp;amount;count(*)\n")
        for i in thread:
            with open(i, 'r') as d:
                for line in d.readlines()[1:]:
                    f.write(line)
def clear_folder():
    for file in os.listdir(os.path.join(os.getcwd(), 'reports')):
        os.remove(os.getcwd() + '/reports/' + file)

def bd_request(start, finish):
        conn = psycopg2.connect(host='msk-dpro-dba200', port='6432', dbname='zabbix', user='zabbix', password='MMn0JK1tO')
        cursor = conn.cursor()
        cursor.execute("""
            select v1.hh1, (v2.maxv-v1.minv) reboot
            from
            (SELECT min(hu1.value) as minv, h1.host as hh1
            FROM history_uint hu1
            join items i1 on hu1.itemid=i1.itemid
            join hosts h1 on h1.hostid=i1.hostid
            where hu1.clock<""" + str(start + 2* 24*3600) + """ and hu1.clock>""" + str(start) + """
            and i1.name='Кол-во перезагрузок за месяц'
            group by h1.host) as v1
            join
            (SELECT max(hu2.value) as maxv, h2.host as hh2
            FROM history_uint hu2
            join items i2 on hu2.itemid=i2.itemid
            join hosts h2 on h2.hostid=i2.hostid
            where hu2.clock<""" + str(finish) + """ and hu2.clock>""" + str(finish - 2* 24*3600) + """
            and i2.name='Кол-во перезагрузок за месяц'
            group by h2.host) as v2 on v2.hh2 = v1.hh1
            order by reboot desc""")
        records = cursor.fetchall()
        week = datetime.now().date().isocalendar()[1] - 1
        with open('reports/' + str(week) + '_week_zabbix_summery.csv', 'w') as f:
            f.write('week {0}\n'.format(week))
            f.write("BO;errors\n")
            for i in records:
                f.write(i[0] + ';' + str(i[1]) + '\n')


def send_data(item):
    for recipient in sys.argv[1:]:
        send_report(subject='UTM errors report',
                    text='',
                    to=recipient,
                    fname='reports/' + item)



if __name__ == '__main__':
    clear_folder()
    start, finish = get_dates()
    hosts = get_zdata()
    init_threads(hosts, start, finish)
    compile()
    bd_request(start, finish)
    for file in os.listdir(os.path.join(os.getcwd(), 'reports')):
        if 'summery.csv' in file:
            send_data(file)
