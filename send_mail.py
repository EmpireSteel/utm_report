import os
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from six.moves import configparser

config = configparser.ConfigParser()
config.read("config/config.cfg")


def send_results(fname):

    with open(fname, "rb") as f:
        content = f.readlines()

    for i, line in enumerate(content):
        if "ERROR" in line:
            content[i] = """<font color="red">%s</font>""" % line
        elif "WARNING" in line:
            content[i] = """<font color="#ff6600">%s</font>""" % line
        elif "<<<" in line or ">>>" in line:
            content[i] = """<font color="#b0b0b0">%s</font>""" % line
        content[i] = content[i].decode('utf')

    html = '''<pre>%s</pre>''' % "".join(content)
    msg = MIMEText(html, 'html', _charset="utf")
    msg['Subject'] = 'ZBX Actualizer results (%s)' % os.path.basename(fname)
    msg['To'] = config.get("email", "recipients")

    server = smtplib.SMTP(config.get("email", "smtp_server"), config.get("email", "smtp_port"))

    server.sendmail("zabbix-tc5@x5.ru", msg['To'], msg=msg.as_string())


def send_report(subject, text, fname, to):
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['To'] = to
    msg.attach(MIMEText(text))

    with open(fname, 'rb') as f:
        f_content = f.read()
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(f_content)
        part.add_header('Content-Disposition', 'attachment', filename=os.path.basename(fname))
        msg.attach(part)

    server = smtplib.SMTP(config.get("email", "smtp_server"), config.get("email", "smtp_port"))
    server.sendmail("zabbix-tc5@x5.ru", msg['To'], msg=msg.as_string())
